package com.techu.apitechu.controller;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseURL  = "/apitechu/v1";

    @GetMapping(APIBaseURL + "/products")
    public ArrayList<ProductModel> getProducts() {

        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseURL + "/products/{id}")
    public ProductModel getProductById (@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("Es el id " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
            }
        }
        return result;
    }

    @PostMapping (APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("El id del nuevo producto es " + newProduct.getId());
        System.out.println("La desc del nuevo producto es " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;

    }

    @PutMapping (APIBaseURL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("El id del producto a actualizar en parámetro url es " + id);
        System.out.println("El id del producto a actualizar es " + product.getId());
        System.out.println("La desc del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels)
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        return product;
    }

    @PatchMapping (APIBaseURL + "/products/{id}")
    public ProductModel updateDescProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("updateDescProduct");
        System.out.println("El id del producto a actualizar en parámetro url es " + id);
        System.out.println("La desc del producto a actualizar es " + product.getDesc());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels)
            if (productInList.getId().equals(id)) {

                result = productInList;

                if (product.getDesc() != null) {

                    productInList.setDesc(product.getDesc());
                }

                if (product.getPrice() != 0) {

                    productInList.setPrice(product.getPrice());
                }
            }


        return result;
    }

    @DeleteMapping (APIBaseURL + "/products/{id}")
    public ProductModel deleteProduct (@PathVariable String id) {

        System.out.println("deleteProduct");
        System.out.println("El id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundProduct = false;
        for (ProductModel product: ApitechuApplication.productModels){
            if (product.getId().equals(id)) {
                System.out.println("Producto para borrar encontrado");
                foundProduct = true;
                result = product;
            }

        }

        if(foundProduct == true)

    {
        System.out.println("Borrando producto" + id);
        ApitechuApplication.productModels.remove(result);
    }
        return new ProductModel();
    }
}
